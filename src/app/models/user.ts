export class User {
    public id: number;
    public username: string;
    public email: string;
    public password_hash: string;
    public password: string;
    public status: string;

    // constructor(
    //     public id: number,
    //     public username: string,
    //     public email: string,
    //     public password_hash: string,
    //     public password: string,
    //     public status: string,
    // ) {}
}
