import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';


import { AppComponent } from './app.component';
import { UserIndexComponent } from './components/user-index/user-index.component';
import { UserManageService } from './services/user-manage.service';
import { AppRoutingModule } from './app-routing.module';
import { UserViewComponent } from './components/user-view/user-view.component';
import { UserCreateComponent } from './components/user-create/user-create.component';


@NgModule({
  declarations: [
    AppComponent,
    UserIndexComponent,
    UserViewComponent,
    UserCreateComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
  ],
  providers: [UserManageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
