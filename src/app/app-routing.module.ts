import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserIndexComponent } from './components/user-index/user-index.component';
import { UserViewComponent } from './components/user-view/user-view.component';
import {UserCreateComponent} from './components/user-create/user-create.component';

const routes: Routes = [
    { path: 'users', component: UserIndexComponent },
    { path: 'detail/:id', component: UserViewComponent },
    { path: 'create', component: UserCreateComponent },
];

@NgModule({
    imports: [ RouterModule.forRoot(routes) ],
    exports: [ RouterModule ]
})
export class AppRoutingModule { }
