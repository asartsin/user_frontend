import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { User } from '../models/user';

const httpOptions = {
     // headers: new HttpHeaders({ 'Content-Type': 'multipart/form-data' })
};

@Injectable()
export class UserManageService {
    private usersUrl = 'api/v1/users';  // URL to web api
    private updateUrl = 'api/v1/user/update?id=';  // URL to web api
    private deleteUrl = 'api/v1/user/delete?id=';  // URL to web api
    private createUrl = 'api/v1/user/create';  // URL to web api

    constructor(
        private http: HttpClient,
        ) { }

    /** GET Users from the server */
    getUsers (): Observable<User[]> {
        return this.http.get<User[]>(this.usersUrl)
            .pipe(
                // tap(useres => this.log(`fetched useres`)),
                catchError(this.handleError('getUsers', []))
            );
    }

    /** GET user by id. Will 404 if id not found */
    getUser(id: number): Observable<User> {
        const url = `${this.usersUrl}/${id}`;
        return this.http.get<User>(url).pipe(
            // tap(_ => this.log(`fetched user id=${id}`)),
            catchError(this.handleError<User>(`getUser id=${id}`))
        );
    }

    /** POST: update the user on the server */
    updateUser (user: User): Observable<any> {
        const url = `${this.updateUrl}${user.id}`;
        const formData = new FormData();
        formData.append('username', user.username);
        formData.append('email', user.email);
        return this.http.post(url, formData, httpOptions).pipe(
            // tap(_ => this.log(`updated user id=${user.id}`)),
            catchError(this.handleError<any>('updateUser'))
        );
    }

    /** POST: add a new user to the server */
    createUser (user: User): Observable<User> {
        const formData = new FormData();
        formData.append('username', user.username);
        formData.append('email', user.email);
        formData.append('password', user.password);
        formData.append('status', user.status);
        return this.http.post<User>(this.createUrl, formData, httpOptions).pipe(
            // tap((user: User) => this.log(`added user w/ id=${user.id}`)),
            catchError(this.handleError<User>('createUser'))
        );
    }

    /** DELETE: delete the user from the server */
    deleteUser (user: User | number): Observable<User> {
        const id = typeof user === 'number' ? user : user.id;
        const url = `${this.deleteUrl}${id}`;

        return this.http.delete<User>(url, httpOptions).pipe(
            // tap(_ => this.log(`deleted user id=${id}`)),
            catchError(this.handleError<User>('deleteUser'))
        );
    }

    /**
     * Handle Http operation that failed.
     * Let the app continue.
     * @param operation - name of the operation that failed
     * @param result - optional value to return as the observable result
     */
    private handleError<T> (operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {

            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead

            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }
}
