import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { User } from '../../models/user';
import { UserManageService } from '../../services/user-manage.service';

@Component({
  selector: 'app-user-view',
  templateUrl: './user-view.component.html',
  styleUrls: ['./user-view.component.css']
})
export class UserViewComponent implements OnInit {
    user: User;

    constructor(
        private route: ActivatedRoute,
        private userService: UserManageService,
        private location: Location
    ) {}

    ngOnInit(): void {
        this.getUser();
    }

    getUser(): void {
        const id = +this.route.snapshot.paramMap.get('id');
        this.userService.getUser(id)
            .subscribe(user => this.user = user);
    }

    goBack(): void {
        this.location.back();
    }

    save(): void {
        this.userService.updateUser(this.user)
            .subscribe(() => this.goBack());
    }

    delete(): void {
        this.userService.deleteUser(this.user)
            .subscribe(() => this.goBack());
    }

}
