import { Component, OnInit } from '@angular/core';

import { User } from '../../models/user';
import { UserManageService } from '../../services/user-manage.service';

@Component({
    selector: 'app-user-index',
    templateUrl: './user-index.component.html',
    styleUrls: ['./user-index.component.css']
})
export class UserIndexComponent implements OnInit {
    users: User[];

    constructor(private userService: UserManageService) { }

    ngOnInit() {
        this.getUsers();
    }

    getUsers(): void {
        this.userService.getUsers()
            .subscribe(users => this.users = users);
    }
}
