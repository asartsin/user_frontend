import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { User } from '../../models/user';
import { UserManageService } from '../../services/user-manage.service';

@Component({
  selector: 'app-user-create',
  templateUrl: './user-create.component.html',
  styleUrls: ['./user-create.component.css']
})
export class UserCreateComponent implements OnInit {

    user: User;

    constructor(
        private route: ActivatedRoute,
        private userService: UserManageService,
        private location: Location
    ) {}

    ngOnInit() {
      this.user = new User();
    }

    add(): void {
      this.userService.createUser(this.user)
          .subscribe(() => this.goBack());
    }

    goBack(): void {
        this.location.back();
    }

}
